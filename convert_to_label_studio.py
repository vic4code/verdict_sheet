import json
from tqdm import tqdm

if __name__ == "__main__":
    
    """
    [
        {
            # "data" must contain the "my_text" field defined in the text labeling config as the value and can optionally include other fields
            "data": {
                "my_text": "Opossums are great",
                "ref_id": 456,
                "meta_info": {
                    "timestamp": "2020-03-09 18:15:28.212882",
                    "location": "North Pole"
                    } 
            },

            # annotations are not required and are the list of annotation results matching the labeling config schema
            "annotations": [{
                "result": [{
                    "from_name": "sentiment_class",
                    "to_name": "message",
                    "type": "choices",
                    "readonly": false,
                    "hidden": false,
                    "value": {
                        "choices": ["Positive"]
                    }
                }]
            }] 
        }
    ]
    """
    

    verdict_sheet_path = f"/Users/cfh00886479/projects/github/verdict_sheet/data.json"
    utc_result_path = f"/Users/cfh00886479/projects/github/verdict-cls-debug/utc/zero_shot_text_classification/inference_results/data_8000/inference_results.json"
    
    with open(verdict_sheet_path, "r", encoding="utf-8") as f:
        original_data = json.load(f)

    with open(utc_result_path, "r", encoding="utf-8") as f:
        utc_result_data = [json.loads(line) for line in f]


    original_data_with_jid = {}

    for data in original_data:
        original_data_with_jid[data["jid"]] = data["jfull_compress"]

    label_studio_data = []

    for data in tqdm(utc_result_data):

        label_studio_data.append(
            {   
                "id": data["id"],
                # "data" must contain the "my_text" field defined in the text labeling config as the value and can optionally include other fields
                "data": {
                    "text": original_data_with_jid[data["jid"]],
                    "jid": data["jid"] 
                },
                # annotations are not required and are the list of annotation results matching the labeling config schema
                "annotations": [{
                    "result": [{
                        "value": {
                            "choices": [item[0] for item in data["pred_labels"].items() if item[1] == 1]
                        },
                        "type": "choices",
                        "from_name": "medical",
                        "to_name": "text",
                        "type": "choices",
                        "origin": "manual"
                    }]
                }] 
            }
        )

    # Save
    with open('./label_studio_text_classification.json', 'w') as f:
        json.dump(label_studio_data, f, ensure_ascii=False)


    cls_data_path = f"/Users/cfh00886479/projects/github/verdict-cls-debug/utc/zero_shot_text_classification/labelstudio_data/formal_data/classification.json"
    
    # with open(cls_data_path, "r", encoding="utf-8") as f:
    #     cls_data = json.load(f)
    # breakpoint()